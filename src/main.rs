fn return_string() -> String {
    "emacs".to_string()
}

fn main() {
    println!("Hi everyone");
    println!("{}", return_string());
}